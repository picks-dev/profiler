<?php

namespace PickSuite\Profiler;

use function sys_get_temp_dir;
use function uniqid;

class Profiler
{
    public $xhprofQuery;
    private $source;
    private $run;

    public function __construct(string $source)
    {
        $this->source = $source;
        $this->run = uniqid();
        $this->xhprofQuery = [
            'run' => $this->run,
            'source' => $this->source,
            'sort' => 'excl_wt',
        ];
    }

    public function getTargetFile(string $storageDir = null): string
    {
        $storageDir = $storageDir ?: sys_get_temp_dir();
        return "{$storageDir}/{$this->run}.{$this->source}.xhprof";
    }

    public function start(): Profiler
    {
        tideways_xhprof_enable(
            TIDEWAYS_XHPROF_FLAGS_MEMORY |
            TIDEWAYS_XHPROF_FLAGS_MEMORY_MU |
            TIDEWAYS_XHPROF_FLAGS_MEMORY_PMU
        );
        return $this;
    }

    public function stop(): array
    {
        return tideways_xhprof_disable();
    }
}
